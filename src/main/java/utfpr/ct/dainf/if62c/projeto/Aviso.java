package utfpr.ct.dainf.if62c.projeto;

import java.util.TimerTask;

public class Aviso extends TimerTask {
    
  protected final Compromisso compromisso;

  public Aviso(Compromisso compromisso) {
    this.compromisso = compromisso;
  }

  @Override
  public void run() {
    if (compromisso.getAvisos().isEmpty())
       this.cancel();
    else {
      long agora = System.currentTimeMillis();
      long comecaEm = (compromisso.getData().getTime() - agora) / 1000;
      
      // arredondamento
      if (((compromisso.getData().getTime() - agora) % 1000) >= 500)
         comecaEm++;
    
      System.out.println(compromisso.getDescricao() + " começa em " + comecaEm + "s");
      if (comecaEm == 0)
         this.cancel();
    }  
  }
        
}

/*
    1. Modifique a classe Aviso de modo a transformá-la em uma tarefa (TimerTask). 
       A tarefa implementada pela classe Aviso deve exibir uma mensagem associada 
       ao compromisso. Por exemplo, se a descrição do compromisso for "Reunião", 
       a mensagem exibida deve ser "Reunião começa em 120s".
*/