package utfpr.ct.dainf.if62c.projeto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

public class Agenda {
  private final String descricao;
  private final List<Compromisso> compromissos = new ArrayList<>();
  private final Timer timer;

  public Agenda(String descricao) {
    this.descricao = descricao;
    timer = new Timer(descricao);
  }

  public String getDescricao() {
    return descricao;
  }

  public List<Compromisso> getCompromissos() {
    return compromissos;
  }
    
  public void novoCompromisso(Compromisso compromisso) {
    compromissos.add(compromisso);
    Aviso aviso = new AvisoFinal(compromisso);
    compromisso.registraAviso(aviso);
    
    timer.schedule(aviso, compromisso.getData());
  }
    
  /*
    4. Implemente o método novoAviso(Compromisso, int) da classe Agenda. A antecedência é dada em 
       segundos. Este método deve criar um aviso para o compromisso, registrá-lo na lista de avisos do 
       compromisso e agendar a execução do aviso para ocorrer com a antecedência especificada em relação 
       à data do compromisso.
  */
  public void novoAviso(Compromisso compromisso, int antecedencia) {
    Aviso aviso = new Aviso(compromisso);
    compromisso.registraAviso(aviso);
         
    Date dt = new Date(compromisso.getData().getTime()-(antecedencia*1000L)); 
    timer.schedule(aviso, dt);
  }
  
  /*
    5. Implemente o método novoAviso(Compromisso, int, int) da classe Agenda. A antecedência e o intervalo 
       são dados em segundos. Este método deve criar um aviso para o compromisso, registrá-lo na lista de 
       avisos do compromisso e agendar a execução do aviso para ocorrer a primeira vez com a antecedência 
       especificada em relação à data do compromisso e depois a cada intervalo especificado.
  */
  public void novoAviso(Compromisso compromisso, int antecedencia, int intervalo) {
    Aviso aviso = new Aviso(compromisso);
    compromisso.registraAviso(aviso);
         
    Date dt = new Date(compromisso.getData().getTime()-(antecedencia*1000L));        
    timer.schedule(aviso, dt, (intervalo*1000L));
  }

  /*
    6. Implemente o método cancela(Compromisso) da classe Agenda. Este método deve cancelar 
       todos os avisos associados ao compromisso e remover o compromisso da lista de compromissos da agenda.
  */    
  public void cancela(Compromisso compromisso) {
    for (int i=0; i<compromissos.size(); i++) {
      if (compromissos.get(i).equals(compromisso)) {
         for (int j=(compromissos.get(i).getAvisos().size()-1); j>=0; j--) {
           compromissos.get(i).getAvisos().remove(j);
         }
         compromissos.remove(i);
         break;
      }  
    }
  }
  
  /*
    7. Implemente o método cancela(Aviso) da classe Agenda. Este método deve cancelar o aviso especificado e 
       removê-lo da lista de avisos do compromisso.
  */
  public void cancela(Aviso aviso) {
    for (int i=0; i<compromissos.size(); i++) {
      if (compromissos.get(i).equals(aviso.compromisso)) {
         for (int j=0; j<compromissos.get(i).getAvisos().size(); j++) {
           if (compromissos.get(i).getAvisos().get(j).equals(aviso)) {
              compromissos.get(i).getAvisos().remove(j);
              break;
           }  
         }
         break;
      }  
    }        
  } 
    
  /*
    8. Implemente o método destroi() da classe Agenda. Este método deve interromper todas as operações 
       relacionadas à agenda de modo que o programa possa ser terminado.
  */
  public void destroi() {
    timer.cancel();
    timer.purge();
  }
    
}