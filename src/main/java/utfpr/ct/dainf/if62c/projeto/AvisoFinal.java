package utfpr.ct.dainf.if62c.projeto;

public class AvisoFinal extends Aviso {

  public AvisoFinal(Compromisso compromisso) {
    super(compromisso);
  }

  /* 
    2. Na classe AvisoFinal, derivada da classe Aviso, faça com que a tarefa implementada 
       pela classe AvisoFinal exiba uma mensagem associada ao compromisso. Por exemplo, se a 
       descrição do compromisso for "Reunião", a mensagem exibida deve ser "Reunião começa agora".

    3. Faça com que a tarefa implementada pela classe AvisoFinal, além de exibir a mensagem 
       conforme o item 2, também cancele todos os avisos relacionados ao compromisso. 
       Isto é necessário já que o aviso final indica que o horário do compromisso foi 
       atingido e qualquer aviso remanescente deve ser suprimido.
  */
  @Override
  public void run() {
    System.out.println(compromisso.getDescricao()+ " começa agora");

    // cancelando todos os avisos relacionados ao compromisso
    for (int i=(compromisso.getAvisos().size()-1); i>=0; i--) {  
      compromisso.getAvisos().remove(i);
    }
  }
    
}